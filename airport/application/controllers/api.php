<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    

class Api extends CI_Controller {

    
    private $errors;
    private $methods;
    private $tokenmethods;
    private $errorCode;
    
    public function __construct() {
        parent::__construct();
        
        $this->errors = $this->config->item('errors');
        $this->load->model('api_model','API');
        $this->methods = array("login","register","search_airport","add_airport","delete_airport");
        $this->tokenmethods = array("search_airport","add_airport","delete_airport");
    }
    
    public function process() {
        
        $this->errorCode = '';        
        $method = strtolower($this->cleaner($this->input->get('method')));// make it lower case
            
        if (in_array($method, $this->methods)) {
            if (in_array($method, $this->tokenmethods)){
                /* token check ; token is required for this method */
                $token = $this->cleaner($this->input->get('token'));
                
                if (!isset($token) || $token == "") {
                    $this->errorCode = 'E010';
                    $this->out('', $this->errorCode);
                } else {
                    /* go get id and email from model to check token's validity */
                    
                    $data['user_id'] = $this->cleaner($this->input->get('user_id'));
                    if (!isset($data['user_id']) || $data['user_id'] == "") {
                        $this->errorCode = 'E011';
                        $this->out('', $this->errorCode);
                        exit();
                    }
                    $user_check = $this->API->get_single_where("users","id",$data["user_id"]);

                    if (!isset($user_check[0]) || count($user_check[0])== 0 ) {
                        $this->errorCode = 'E012';
                        $this->out('', $this->errorCode);
                        exit();
                    }

                    $newToken = md5($data['user_id'] . $user_check[0]["username"].$user_check[0]["email"]);
                    
                   
                    if ($newToken !== $token) {
                        $this->errorCode = 'E013';
                        $this->out('', $this->errorCode);
                    } else {
//			echo 'gud job';
                        $this->$method();
                    }
                }
            } else {

                $this->$method();
            }
        } else {
            $this->errorCode = 'E013';
            $this->out('', $this->errorCode);
            exit();
        }
    }    
    
    
    
	public function index()
	{
		$this->load->view('welcome_message');
	}

        
    function login() {
           
        $data['email'] = $this->cleaner($this->input->post('email'));
                
        $data['password'] = $this->cleaner($this->input->post('password'));

        if (!isset($data['password']) || $data['password'] == '') {
            $this->errorCode = 'E005';
            $this->out('', $this->errorCode);
            exit();
        }
        if (!isset($data['email']) || $data['email'] == '') {
            $this->errorCode = 'E004';
            $this->out('', $this->errorCode);
            exit();
        }
        if (!$this->validEmail($data['email'])) {
            $this->errorCode = 'E006';
            $this->out('', $this->errorCode);
            exit();
        }
        
        $data['password'] = md5($data['password']);
        //////////////////////////
        $myresult = $this->API->get_single_join("user_roles.role as user_role,users.id as user_id,users.email as email, users.username as username ","users","user_roles","users.id","user_roles.user_id","users.email",$data['email'],"users.password",$data['password']);


$result_array = array();
        if($myresult[0]["user_id"]){
            $result_array['status'] = 'login successfull';
            $result_array['token'] = md5($myresult[0]["user_id"] . $myresult[0]["username"].$myresult[0]["email"]);
            $result_array['user_id'] = $myresult[0]["user_id"];
            $result_array['user_role'] = $myresult[0]["user_role"];
                
        }else{
            $result_array['status'] = 'login failed';
        }        
        
        $this->out($result_array);
        exit;
        
    }    
    
    function register() {

        $data2['role'] = strtolower($this->cleaner($this->input->get('role')));        
        if (!isset($data2['role']) || $data2['role'] == '') {
            $this->errorCode = 'E008';
            $this->out('', $this->errorCode);
            exit;
        }
        
        if ($data2['role'] != "guest" && $data2['role'] !=  "user" && $data2['role'] != "admin") { // make lower case
            $this->errorCode = 'E009';
            $this->out('', $this->errorCode);
            exit;
        }
        
        $data['username'] = $this->cleaner($this->input->post('username'));        
        $data['email'] = $this->cleaner($this->input->post('email'));        
        $data['password'] = $this->cleaner($this->input->post('password'));        
                        
        if (!isset($data['username']) || $data['username'] == '') {
            $this->errorCode = 'E002';
            $this->out('', $this->errorCode);
            exit;
        }
        if (!isset($data['email']) || $data['email'] == '') {
            $this->errorCode = 'E004';
            $this->out('', $this->errorCode);
            exit;
        }
        if (!$this->validEmail($data['email'])) {
            $this->errorCode = 'E006';
            $this->out('', $this->errorCode);
            exit;
        }
        
        
        
        if (!isset($data['password']) || $data['password'] == '') {
            $this->errorCode = 'E005';
            $this->out('', $this->errorCode);
            exit;
        }
        
        $username_check = $this->API->get_single_where("users","username",$data['username']);
        
        if (count($username_check) > 0) {
            $this->errorCode = 'E003';
            $this->out('', $this->errorCode);
            exit;
        }
        
        $email_check = $this->API->get_single_where("users","email",$data['email']);
        if (count($email_check) > 0) {
            $this->errorCode = 'E007';
            $this->out('', $this->errorCode);
            exit;
        }        
        $data['password'] = md5($data['password']);
        //$data['token'] = md5($data['full_name'] . $data['username'].$data['gender']);
        $data2['user_id'] = $this->API->simple_insert("users",$data);
        $role_id = $this->API->simple_insert("user_roles",$data2);        
        $result_array = array();
        if($data2['user_id']){
            $result_array['status'] = 'registration successfull';
            $result_array['token'] = md5($data2['user_id'] . $data['username'].$data['email']);
            $result_array['user_id'] = $data2['user_id'];
            $result_array['user_role'] = $data2['role'];
                
        }else{
            $result_array['status'] = 'registration failed';
        }        
        $this->out($result_array);       
        exit;
        
        
    }
    
    
    function add_airport(){
        $user_id = $this->cleaner($this->input->get('user_id'));
        $data2['user_id'] = $this->cleaner($this->input->get('user_id'));
        $data['airport_name'] = $this->cleaner($this->input->get('airport_name'));
        $data['country'] = $this->cleaner($this->input->get('country'));
        $data['city'] = $this->cleaner($this->input->get('city'));
        $user_role = $this->API->get_single_where("user_roles","user_id",$user_id);
        $user_role = $user_role[0]['role'];
        if (!isset($user_role) || $user_role == '') {
            $this->errorCode = 'E008';
            $this->out('', $this->errorCode);
            exit;
        }
        if (!isset($data['airport_name']) || $data['airport_name'] == '') {
            $this->errorCode = 'E016';
            $this->out('', $this->errorCode);
            exit;
        }
        if (!isset($data['country']) || $data['country'] == '') {
            $this->errorCode = 'E017';
            $this->out('', $this->errorCode);
            exit;
        }
        if (!isset($data['city']) || $data['city'] == '') {
            $this->errorCode = 'E018';
            $this->out('', $this->errorCode);
            exit;
        }

        $data['airport_code'] = $this->random_password(8); 
        
        
        if($user_role == "guest" || $user_role == "user" || $user_role == "admin"){
            
            if($user_role == "admin"){
                $data2['airport_id'] = $this->API->simple_insert("airport",$data);
                $temp = $this->API->simple_insert("user_airport_relation",$data2);    
                $result = array();
                $result["status"] = "airport added successully";
                $this->out($result);
            }elseif($user_role == "user"){                
                $query = "select airport.city as city from  user_roles join user_airport_relation on user_roles.user_id = user_airport_relation.user_id join airport on user_airport_relation.airport_id = airport.id where user_roles.user_id = ".$data2['user_id']." AND user_roles.role = 'user' ; ";
                $response = $this->API->run_query($query);
                if(count($response[0]["city"])){
                    $this->errorCode = 'E019';
                    $this->out('', $this->errorCode);
                    exit;
                }else{
                     $data2['airport_id'] = $this->API->simple_insert("airport",$data);
                     $temp = $this->API->simple_insert("user_airport_relation",$data2);  
                     $result = array();
                     $result["status"] = "airport added successully";
                     $this->out($result);
                }
                
            }else{
                $this->errorCode = 'E019';
                $this->out('', $this->errorCode);
                exit;
            }
            
        }else{
            $this->errorCode = 'E009';
            $this->out('', $this->errorCode);            
        }
    }
    
    
    function search_airport(){
        $user_id = $this->cleaner($this->input->get('user_id'));
        $user_role = $this->API->get_single_where("user_roles","user_id",$user_id);
        $user_role = $user_role[0]['role'];
        if (!isset($user_role) || $user_role == '') {
            $this->errorCode = 'E008';
            $this->out('', $this->errorCode);
            exit;
        }

        $airport_code = $this->cleaner($this->input->get('airport_code'));
        if (!isset($airport_code) || $airport_code == '') {
            $this->errorCode = 'E014';
            $this->out('', $this->errorCode);
            exit;
        }
        
        if($user_role == "guest" || $user_role == "user" || $user_role == "admin"){
            $response = array();
            $airport_code_check = $this->API->get_single_where("airport","airport_code",$airport_code) ;
            
            if($user_role == "user" || $user_role == "admin"){
                
                if(isset($airport_code_check[0]) && count($airport_code_check[0]) ){
                    $response = $airport_code_check[0];                    
                }else{
                    $this->errorCode = 'E015';
                    $this->out('', $this->errorCode);
                    exit;
                }            
                
            }else{
                // for guest ; I think i'll have to log time for guest user but I'mout of time so leaving that part
                if(isset($airport_code_check[0]) && count($airport_code_check[0]) ){
                    $response = $airport_code_check[0];                    
                }else{
                    $this->errorCode = 'E015';
                    $this->out('', $this->errorCode);
                    exit;
                }            
                
            }            
        }else{
            $this->errorCode = 'E009';
            $this->out('', $this->errorCode);            
        }
        $this->out($response);
        
    }
    function delete_airport(){
        $user_id = $this->cleaner($this->input->get('user_id'));
        $airport_code = $this->cleaner($this->input->get('airport_code'));
        if (!isset($airport_code) || $airport_code == '') {
            $this->errorCode = 'E014';
            $this->out('', $this->errorCode);
            exit;
        }
        $user_role = $this->API->get_single_where("user_roles","user_id",$user_id);
        if(isset($user_role[0]['role']) && $user_role[0]['role'] == "admin" ){
            $airport_data = $this->API->get_single_where("airport","email",$data['email']);
            if(isset($airport_data[0]['id'])){
                $this->API->delete_where("airport","airport_code",$airport_code);          
                $this->API->delete_where("user_airport_relation","id",$airport_data[0]['id']);
                     $result = array();
                     $result["status"] = "airport has been deleted successully";
                     $this->out($result);
            }
        }else{
            $this->errorCode = 'E019';
            $this->out('', $this->errorCode);  
        }
        
        
        
        
    }
    
    
    
    
        
    public function cleaner($input){
        $input = trim($input);
        $input = strip_tags($input);
        return $input;
        
    }
    

    public function out($data_array = array(), $errorCode = '') {
        if ($this->errorCode != '') {
            $result_array['http_status'] = $this->errorCode;
            $result_array['error'] = $this->errors[$this->errorCode];
        } else {
            $result_array['http_status'] = '200';
            $result_array['data'] = $data_array;
        }
        header('Content-Type: application/json');
        echo json_encode($result_array);
        exit;
    }
    
    public function validEmail($address) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
    }
        
    function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
        
        
        
}
